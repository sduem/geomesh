
%A karst cave model is constructed from the point cloud data

%% Extract the contour of point cloud data
% ptCloud2Curve;

%% 3D model initialization
Comsol_with_Matlab_Start;
import com.comsol.model.util.*
model = ModelUtil.create('Model1');% ModelUtil.remove('Model');
model.modelNode.create('mod1');
model.geom.create('geom1', 3);
model.mesh.create('mesh1', 'geom1');
data_dir = pwd ;

%% Constructed Irregular 3D volumes from 2D contour curves
model = lofting(model,data_dir);

model.component('mod1').geom('geom1').create('pare1', 'PartitionEdges');
model.component('mod1').geom('geom1').feature('pare1').selection('edge').set({'ipc1(1)' 'ipc2(1)' 'ipc3(1)' 'ipc4(1)' 'ipc5(1)' 'ipc6(1)' 'ipc7(1)' 'ipc8(1)' 'ipc9(1)' 'ipc10(1)'  ...
'ipc11(1)' 'ipc12(1)' 'ipc13(1)' 'ipc14(1)' 'ipc15(1)' 'ipc16(1)' 'ipc17(1)' 'ipc18(1)' 'ipc19(1)' 'ipc20(1)'  ...
'ipc21(1)' 'ipc22(1)' 'ipc23(1)' 'ipc24(1)' 'ipc25(1)' 'ipc26(1)' 'ipc27(1)' 'ipc28(1)' 'ipc29(1)' 'ipc30(1)'}, [1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2; 1 2]);
model.component('mod1').geom('geom1').create('pare2', 'PartitionEdges');
model.component('mod1').geom('geom1').feature('pare2').selection('edge').set({'pare1(1)' 'pare1(2)' 'pare1(3)' 'pare1(4)' 'pare1(5)' 'pare1(6)' 'pare1(7)' 'pare1(8)' 'pare1(9)' 'pare1(10)'  ...
'pare1(11)' 'pare1(12)' 'pare1(13)' 'pare1(14)' 'pare1(15)' 'pare1(16)' 'pare1(17)' 'pare1(18)' 'pare1(19)' 'pare1(20)'  ...
'pare1(21)' 'pare1(22)' 'pare1(23)' 'pare1(24)' 'pare1(25)' 'pare1(26)' 'pare1(27)' 'pare1(28)' 'pare1(29)' 'pare1(30)'}, [1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4; 1 2 3 4]);
model.component('mod1').geom('geom1').create('pare3', 'PartitionEdges');
model.component('mod1').geom('geom1').feature('pare3').selection('edge').set({'pare2(1)' 'pare2(2)' 'pare2(3)' 'pare2(4)' 'pare2(5)' 'pare2(6)' 'pare2(7)' 'pare2(8)' 'pare2(9)' 'pare2(10)'  ...
'pare2(11)' 'pare2(12)' 'pare2(13)' 'pare2(14)' 'pare2(15)' 'pare2(16)' 'pare2(17)' 'pare2(18)' 'pare2(19)' 'pare2(20)'  ...
'pare2(21)' 'pare2(22)' 'pare2(23)' 'pare2(24)' 'pare2(25)' 'pare2(26)' 'pare2(27)' 'pare2(28)' 'pare2(29)' 'pare2(30)'}, [1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8; 1 2 3 4 5 6 7 8]);

% model.component('mod1').geom('geom1').create('loft1', 'Loft');
model.component('mod1').geom('geom1').feature('loft1').set('facepartitioning', 'grid');
model.component('mod1').geom('geom1').feature('loft1').selection('profile').set({'pare3'});
model.component('mod1').geom('geom1').feature('loft1').selection('guide').set({});
model.component('mod1').geom('geom1').run;

mphlaunch(model)
mphsave(model,'cavetest');





