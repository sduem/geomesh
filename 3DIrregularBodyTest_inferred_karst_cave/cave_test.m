%A karst cave model is constructed from the point cloud data

%Extract the contour of point cloud data
ptCloud2Curve;

%3D model initialization
Comsol_with_Matlab_Start;
import com.comsol.model.util.*
model = ModelUtil.create('Model1');% ModelUtil.remove('Model');
model.modelNode.create('mod1');
model.geom.create('geom1', 3);
model.mesh.create('mesh1', 'geom1');
data_dir = pwd ;

%Constructed Irregular 3D volumes from 2D contour curves
model = lofting(model,data_dir);
mphsave(model,'cave');