function [object_blk,blk_pos_corner ]=AddBlocks(model,blk)
%

blk_pos_corner = [];
ins0 = 0;
for n = 1:size(blk.Lx,2)
    %Size unit：m
    lenx = blk.Lx(n);
    leny = blk.Ly(n);
    lenz = blk.Lz(n);
    %Center position coordinate
    xc = blk.CentCoord(n,1);
    yc = blk.CentCoord(n,2);
    zc = blk.CentCoord(n,3);

    blk_position = [xc yc zc];
    blk_size = [lenx leny lenz];
    blkLabel = ['blk' num2str(n+ins0)];
     
    model.geom('geom1').feature.create(blkLabel,'Block');
    model.geom('geom1').feature(blkLabel).set('size',blk_size);
    model.geom('geom1').feature(blkLabel).set('pos',blk_position);
    model.geom("geom1").feature(blkLabel).set("rot", blk.angle);
    model.component("mod1").geom("geom1").feature( blkLabel).set("base", "center");

    object_blk{n}  = blkLabel;
    temp = [];
    blk_pos_corner  = cat(1, blk_pos_corner, temp);
end

model.component("mod1").geom("geom1").run();


