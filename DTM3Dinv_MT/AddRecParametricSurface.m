function [object_rec,ps_pos_rec ]=AddRecParametricSurface(model,rec,lengthcurve,heightcurve)
% Parametric Surface  receive

% ins0 = 0;
% xrec=[2 10];
% yrec=[0 0];
% zrec=[0 0];
% lengthcurve=[5 5];
% heightcurve=5;

x = rec(:,1);
y = rec(:,2);
z = rec(:,3);
nrec = length(x);
object_rec=cell(1,nrec);%{};
ps_pos_rec=[];
ins0 = 0;
for n = 1:nrec
    psLabel = ['ps' num2str(n+ins0)];
    ps(n+ins0)= model.geom('geom1').feature.create( psLabel ,'ParametricSurface');
    model.geom('geom1').feature( psLabel ).set('parmin1',num2str(x(n)-lengthcurve/2));
    model.geom('geom1').feature( psLabel ).set('parmin2',num2str(z(n)-heightcurve));
    model.geom('geom1').feature( psLabel ).set('parmax1',num2str(x(n)+lengthcurve/2));
    model.geom('geom1').feature( psLabel ).set('parmax2',num2str(z(n)+heightcurve));
    model.geom('geom1').feature( psLabel ).set('coord',{'s1',num2str(y(n)),'s2'});
    model.geom('geom1').feature( psLabel ).set('maxknots',{'4'});
    object_rec{n}  = psLabel;
    ps_pos_temp = [x(n)-lengthcurve/2 y(n) z(n)-heightcurve x(n)-lengthcurve/2 y(n) z(n)+heightcurve];
    ps_pos_rec  = cat(1, ps_pos_rec, ps_pos_temp);
end
model.component("mod1").geom("geom1").run();


