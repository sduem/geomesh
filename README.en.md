# GeoMesh

#### Description
This project was developed by the Laboratory of Earth Electromagnetic Exploration, Shandong University, and is dedicated to developing efficient modeling workflows suitable for complex geophysical models.


#### Software Architecture

1. Program language: MATLAB
2. Software required: MATLAB and COMSOL
3.  Operating system: Windows, Linux, MacOS


#### Installation

1.  Installing MATLAB
2.  Installing COMSOL The authors recommend installing the latest version of the software.
3.  Run the script to generate the models.

#### Instructions

1. Start COMSOL Multiphysics with MATLAB.
2. Change the lib file path.
3. Running MATLAB scripts.


#### Contribution




#### Gitee Feature

The paper was submitted to  _Computers & Geosciences_  under the title "Efficient geophysical modeling techniques for complex geological structures".

