# GeoMesh

#### 介绍 Introduction
 This project was developed by the Laboratory of Earth Electromagnetic Exploration, Shandong University, and is dedicated to developing efficient modeling workflows suitable for complex geophysical models.

#### 开发环境与代码说明 Software architecture

1.  Program language: MATLAB 
2.  Software required: MATLAB and COMSOL
3.  Operating system: Windows

#### 安装教程 Installation

1.  Installing MATLAB 
2.  Installing COMSOL
 
The authors recommend installing the latest version of the software.

#### 运行说明 Run Instructions

1.  Start COMSOL Multiphysics with MATLAB.
2.  Change the lib file path.
3.  Running MATLAB scripts.

#### 参与贡献

#### 如何引用 How to Cite

Minghong Liu, Huaifeng Sun, Rui Liu, Liqiang Hu, Ruijin Kong, Shangbin Liu*, 2024, Efficient geo-electromagnetic modeling techniques for complex geological structures: A karst MT example, Computers & Geosciences, Volume 185, 105557

https://www.sciencedirect.com/science/article/pii/S0098300424000402?dgcid=coauthor#abs0015
