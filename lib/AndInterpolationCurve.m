function objectIPC = AndInterpolationCurve(model,filenames)
%Interpolating curves are added, which can be used to construct 3D irregular volumes
for n = 1:size(filenames,1)
    IPCname =['ipc' num2str(n)];
    model.geom('geom1').create(IPCname, 'InterpolationCurve');
    model.geom('geom1').feature(IPCname).set('type', 'closed');
    model.geom('geom1').feature(IPCname).set('source', 'file');
    model.geom('geom1').feature(IPCname).set('filename', filenames{n});
    model.geom('geom1').feature(IPCname).set('struct', 'sectionwise');
    objectIPC{n} = IPCname;
end
model.geom('geom1').run;

