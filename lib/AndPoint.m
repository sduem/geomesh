
function objectIPC = AndPoint(model,p)
% Add points that can be used for the endpoints of 3D irregular volumes
for np = 1:size(p,1)
    PTname =['pt' num2str(n)];
    model.geom("geom1").create(PTname, "Point");
    model.geom("geom1").feature(PTname).setIndex("p", p(np,1), 0);
    model.geom("geom1").feature(PTname).setIndex("p", p(np,2), 1);
    model.geom("geom1").feature(PTname).setIndex("p", p(np,3), 2);
    objectPT{n} =  PTname;
end
model.geom('geom1').run;
end
