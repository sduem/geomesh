

% Start the 'COMSOL Multiphysics with MATLAB' interfaces
% Required: '...\comsolmphserver.exe' and '...\Multiphysics\mli' file address 
% You can also manually launch the executable COMSOL Multiphysics with MATLAB. exe

path = pwd;

try
    mphtags -show
    warning('Already connected to a server!');
catch
    winopen('D:\Software\COMSOL60\Multiphysics\bin\win64\comsolmphserver.exe');
    cd 'D:\Software\COMSOL60\Multiphysics\mli';
    mphstart;
end
cd(path);


