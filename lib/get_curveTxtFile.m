
%Gets the path of a file with a specific character in the folder
function filenames = get_curveTxtFile(data_dir,id,str)
% data_dir：folder path
% id：The position of the character
% Finding characters
% example: curve_01.txt;curve_02.txt
% CurveFiles =  get_curveTxtFile(data_dir,[1:5],'curve');

D = dir(data_dir );
nf = 0;
for i=3:numel(D)
    if strcmp(D(i).name(id),str)
        nf = nf+1;
        filenames{nf,1} = [data_dir '\' D(i).name] ;
    end
end