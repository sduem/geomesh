function model = lofting(model,data_dir)
%Constructed Irregular 3D volumes from 2D contour curves
% example:
% Comsol_with_Matlab_Start;
% import com.comsol.model.util.*
% model = ModelUtil.create('Model1');% ModelUtil.remove('Model');
% model.modelNode.create('mod1');
% model.geom.create('geom1', 3);
% model.mesh.create('mesh1', 'geom1');
% data_dir = pwd ;
% model = lofting(model,data_dir)

CurveFiles =  get_curveTxtFile(data_dir,[1:5],'curve');
objectIPC = AndInterpolationCurve(model,CurveFiles);

model.geom("geom1").create("loft1", "Loft");
model.geom("geom1").feature("loft1").selection("profile").set(objectIPC);
model.geom("geom1").feature("loft1").set("facepartitioning", "grid");

% model.geom("geom1").create("pare1", "PartitionEdges");

try
model.component("mod1").geom("geom1").run();
catch
    warning('The automatic lofting failed, so the Partition Edges had to be added manually.');
end
 mphlaunch(model);


end