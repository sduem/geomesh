

figure;
xyzID = ['X';'Y';'Z'];

scatter(Pn(:,1),Pn( :,2),3,"filled");
hold on;
plot(Pn(k,1),Pn(k,2),'g--','LineWidth',2);
hold on;
plot(PI(1,:),PI(2,:),'k','LineWidth',2);


title(['Silce' num2str(i)],'FontSize',12,'FontWeight','bold');
xlabel([xyzID(planeID(1)) '(m)']);
ylabel([xyzID(planeID(2)) '(m)']);
l=legend('Point cloud slice','Point cloud boundary','Smooth boundary');
set(l,'Box','off','FontSize',10);
set(gca,'color','none','linewidth',1,'FontSize',12,'FontWeight','bold');
set(gcf,'Position', [713.8000 224.2000 404.8000 361.6000]);
box on;

axis tight
axis equal;
% xlim([80,200]);
% ylim([-120,120]);
% set(gca,'color','none');
% set(gcf,'color','none');
