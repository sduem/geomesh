  % Simple curve
  x = linspace(0,100,200);
  y = cos(x/10)+(x/50).^2 + randn(size(x))/10;
  z = smooth1q(y,[]);
  plot(x,y,'r.',x,z,'k','LineWidth',2)
  axis tight square

  % Periodic curve with ouliers and missing data
  x = linspace(0,2*pi,300);
  y = cos(x)+ sin(2*x+1).^2 + randn(size(x))/5;
  y(150:155) = rand(1,6)*5;
  y(10:40) = NaN;
  subplot(121)
  z = smooth1q(y,1e3,'periodic');
  plot(x,y,'r.',x,z,'k','LineWidth',2)
  axis tight square
  title('Non robust')
  subplot(122)
  z = smooth1q(y,1e3,'periodic','robust');
  plot(x,y,'r.',x,z,'k','LineWidth',2)
  axis tight square
  title('Robust')

  % Lima鏾n
  t = linspace(0,2*pi,300);
  x = cos(t).*(.5+cos(t)) + randn(size(t))*0.05;
  y = sin(t).*(.5+cos(t)) + randn(size(t))*0.05;
  z = smooth1q(complex(x,y),[],'periodic');
  plot(x,y,'r.',real(z),imag(z),'k','linewidth',2)
%   axis equal tight